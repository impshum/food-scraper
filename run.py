from bs4 import BeautifulSoup
import requests
from requests_html import HTMLSession


cookies = {
    'consumer': 'default',
    'DCO': 'wdc',
    '_csrf': 'lgvYFnA6o6fqBfaxLt-W-SJl',
    '_abck': 'CCB09D8BDF7E37FAEEE1F672EAA34BA5~-1~YAAQJFfdWLOqODSOAQAAeSkdgAsXH8s0I7aBCmYYWL3Miper9oQpbjgYfDa1sAEdCRS3nixgAFMWuF+WJP+0UWmozehNxdkhDJp3bpCnpD36XjlebHAMSWX5njWIZLTKKrdjI+mdjFydnCoqvTZ6V4bNP7lqry7HeIeBn/4dOzSKAixNRfQ0qvo5ZL+tMnHrIcv2QsfgRLPACChHV3EKzhed1W0TCFJ5bu2ESKgmWn/nGTk7usMahjBfCr8/uKR5zfp6homWJH7gdxCeYY9Qbu4ItE5n/Y117973hc+LEgHcGKFqWgls0NIk4EeNCiFLSayP2o/fIW2htF5Rf7j4iOiyh8VbHZuCUR6C3cJZOn7hArPb94iG6aeS/HdQxOsCX6/oIFr2kd1pb7DoD/S9tUUn+W2/gjfy~0~-1~1711545668',
    'cookiePreferences': '%7B%22experience%22%3Afalse%2C%22advertising%22%3Afalse%7D',
    'atrc': '02a98ef5-c8c5-47a9-9deb-85e05514e96a',
    'AMCV_E4860C0F53CE56C40A490D45%40AdobeOrg': '1585540135%7CMCIDTS%7C19800%7CMCMID%7C60992108307911667306911000663386440129%7CMCAID%7CNONE%7CMCOPTOUT-1710707582s%7CNONE%7CvVersion%7C4.4.0',
    'gtm.start': '1711549370409',
    'bm_sz': '47579B1E9460F6D305BA928172B45196~YAAQJFfdWCoSMDSOAQAAbRfbfxe38sh2shi+g/BZHMVXq5cPBWwEA+i9EX2sTb/aM2zZr/fRTxx3MMQDnmPdYtaAOfSGMEfBlwWKMdNgZKfe+2qcsRLQPHxF69Djc9UNNzNNPYGVnMBN6PYsX7mwWdzWLvaS/KZ5keXS4kBb+qAf/MF70cEnWXAC7MirUdL9VEIb/8UVCdnJVhMP12oTUqqhvhXUHT6O4xE4Mfq8DqzKbjgvyuwzFPVN7Pgpc2ygfIuPOebskg6DjNSfK5BB+cydWd8LAWc26g1G22ckdTVO2skbtaNLgz4cyEd19P140ffpmAsMxoFJyG2SXvt0Aa+U8oMq7ufECJns9NJLiYkTz1xVCg==~3556151~4469828',
    'akavpau_tesco_groceries': '1711546678~id=cd34025f2221a0d5d078dadade087961',
    '_gcl_au': '1.1.483124315.1711546379',
    'ak_bmsc': '85A5920100A940DF3A46062A9A21EF83~000000000000000000000000000000~YAAQHOQWAjWRK3uOAQAAztJKgBdGWbAqy+u0C8KQoeIuxoT0J0Y0vth9NyEcrxuilYzeWoUaVtc4WowVN1cnQ2RkCQuOJmfYkJnSnfP1ewHtt3WS2zSEI1doDLyY5WNKfd5MjkvgQKgEuQayakqknfoT6lJiTSTrKRR2wADpRwYEjXY+wE8/kRtsHKufba/Lc31ijSv5I2eoWr/K5Hc4e/6dug0XhjTLKAMApJT2dfRnpDhUi0/jcJ1QcGP73+OaH+Ej7JN3bLQ5GyL7p030Bd19G9Vnl5h1dXio1dGZZvhpszpVHJzKw9nQtuLA7OhNrjkTxXuR1uL6Gzw+Ohjl2uRbq7R3Tx50s3ORJ0UT9ZpQ8kfZw+iyXPUq1Zbd92VWktJ8CJbhArnV6jJu9vpvdXk7zuLVbozb4t9eMcLcKjDgMsT7nJTwYZfGyzZyHVYHnPjxFWPJF1ru9zKXHW9DKPNadcZRarF4KW7+H5Oi3DeGNi/rSP+FVgwVLDTxFNBMAeIGPCKmUniIp6cESH3Tw9aQTpdF5cKaAMJuuzsctOLOjc8KWG3dNlTH1JBaP45Z+MBeGtcHcJlsZOD5KeifHA==',
    'bm_mi': '88635F5F1574FF614C3A5D15F0194790~YAAQHOQWApNxK3uOAQAADFlJgBcjHTEtMAuuGbjrwfiwgB+eiS34wnhsfQqUBajuvVMn4Z8Q9mqJsq4+4Hcf0axcp9yejyhpAbIhtqmdxoPS1berjn1iR/TvP8w0yh1grzTYkj/uX65o+dxiVBFcK5adxPKRqSNWcrqpkEMSvNG4f+49wUEUtjuus4a0eahZMcdroA5C4Uda6SiPI6YTgdOE2KICCLJBjYtUDCwHhyHZ4JsafecrbGr1mfB3HiFjp3Qcn5Hp3tQvwhvFz6ZrrmNZ3PTiFzoEnRvKEB4D3N5xMfuKRu6ZWKslFcnqp93Mg0Ez48tKEbQ5SyDCsO52n3a1IbBPTyNZ3SYws1A=~1',
    'bm_sv': '75CF0331799B6C3026F8321B055B64AF~YAAQf3ERAgk82H+OAQAA91tQgBdfYR3xyAiZq8dyd43RpXpBKjKVusHon/34CsqSDJIP04xTQ0j2ClZG8S9pX7ipbL9eKI9C810sHOzVfGh0AF7ucSoHX7RbkVVjmjERIQLoCIuf7IaB72VUxD/2IyjmU2JNaU+GSC3ZdpH7mu2989MJ/AVsO0xc6DUr0+BpPN2kJHgWvfRPVWMp6V2LxHQj/TJGTKGqugLuUbR1GPnfvjSpGli2ZASA074OVHVm~1',
}

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:123.0) Gecko/20100101 Firefox/123.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-GB,en;q=0.5',
    'Referer': 'https://www.tesco.com/groceries/en-GB/products/275280804',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'DNT': '1',
    'Sec-GPC': '1',
}


def lovely_soup(url):
    r = requests.get(url, headers=headers, cookies=cookies)
    return BeautifulSoup(r.content, 'lxml')


store_data = {
    "stores": [
        {
            "active": False,
            "title": "tesco",
            "base_url": "https://www.tesco.com/groceries/en-GB/products/",
            "products": ["275280804", "251627289"]
        },
        {
            "active": True,
            "title": "morrisons",
            "base_url": "https://groceries.morrisons.com/products/",
            "products": ["morrisons-ripe-and-ready-bananas-217855011", "morrisons-apples-620025011"]
        },
        {
            "active": False,
            "title": "asda",
            "base_url": "https://groceries.asda.com/product/",
            "products": ["apples/asda-sweet-juice-royal-gala-apples/910002849510", "bananas/asda-organic-fairtrade-bananas/1000128213094"]
        }
    ]
}


def tesco_product_scraper(soup):
    product_title = soup.select_one('.ddsweb-heading').text.strip()
    product_price_container = soup.select_one('.ddsweb-price__container')
    product_price = product_price_container.select('p')[0].text.strip()
    product_price_per = product_price_container.select('p')[1].text.strip()
    data = {'product_title': product_title, 'product_price': product_price, 'product_price_per': product_price_per}
    return data


def morrisons_product_scraper(soup):
    product_title = soup.select_one('.bop-title h1').text.strip()
    product_price = soup.select_one('h2.bop-price__current').text.strip()
    product_price_per = soup.select_one('span.bop-price__per').text.strip()
    data = {'product_title': product_title, 'product_price': product_price, 'product_price_per': product_price_per}
    return data


def asda_product_scraper(soup):
    product_title = soup.select_one('h1.pdp-main-details__title').text.strip()
    product_price = soup.select_one('strong.pdp-main-details__price').text.strip()
    product_price_per = soup.select_one('span.bop-co-product__price-per-uom').text.strip()
    data = {'product_title': product_title, 'product_price': product_price, 'product_price_per': product_price_per}
    return data


for store in store_data['stores']:
    store_name = store['title']
    base_url = store['base_url']
    products = store['products']
    active = store['active']

    if active:
        for product in products:
            product_url = f'{base_url}{product}'
            product_data = None

            soup = lovely_soup(product_url)

            if store_name == 'tesco':
                product_data = tesco_product_scraper(soup)

            if store_name == 'morrisons':

                product_data = morrisons_product_scraper(soup)

            if store_name == 'asda':
                product_data = asda_product_scraper(soup)

            print(store_name, product_data)
